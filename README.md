# Concurrent ALEX
This repository is a fork of the GRE benchmark for learned indexes found [here](https://github.com/gre4index/GRE). Unecessary modules have been deleted. The ALEX implementation that can be found here is an implementation that supports concurrency using strict synchronization policy implemented with atomic variables.

# Dependencies
The following are needed to compile the code and run the benchmarks. 
```bash
sudo apt-get install intel-mkl-full
sudo apt-get install libjemalloc-dev
sudo apt install libtbb-dev
sudo apt-get install numactl
```

# Downloading datasets
## fb
The `fb` dataset can be dowload by running:
```bash
cd datasets
./download.sh
cd ..
```

## YCSB
The `ycsb` dataset is obtained with
```bash
cd datasets/ycsb-prep
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release .. && make
cd ..
./prep.sh
cd ../..
```

## Uniform datasets
Both `unif-data-500M.bin.data` and `unif-data-10B.bin.data` datasets are obtained with
```bash
cd datasets/unif-datasets
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release .. && make
cd ..
./prep.sh
cd ../..
```

# Compiling
```bash
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release .. && make
```

# Exmaple run
```bash
numactl --membind=0 --cpunodebind=0 ./build/microbench --keys_file=./datasets/fb --keys_file_type=binary --read=1 --insert=0 --operations_num=10000000 --table_size=10000000 --init_table_ratio=1 --thread_num=8 --index=alex --memory=true
```

If your system does not have multiple cpu, you should omit `numactl --membind=0 -cpunodebind=0`.

# Benchmarks
In the `./report/benchmarks-scripts` directory there is a series of bash scripts. They have to be ran form the `alex-concurrent` directory.

The throughput benchmarks data were obtained from `workloads-unif.sh` and `workloads-zipf.sh` which respectively run the uniform benchmark and Zipfian benchmark. 

The memory benchmarks were obtained with `memory-concurrent.sh`. To obtain the benchmark for the original ALEX, simply clone the GRE repo from [here](https://github.com/gre4index/GRE.git), follow their README to compile the code (there is a copy of it here called `README_GRE.md`). The `memory-original-ALEX.sh` can be ran from the directory of the cloned repo. 

The stress test on large datasets can be ran with `stress-test.sh`.

All of the above scripts take two parameters, the first one is a Gitlab token for autosaving on a repo and the second one is the name that will be used to create the csv file that will receive the data. It is necessary to update the `repoId` and `gitlabUrl` variables to your personal repository information and to make sure the repository contains the empty csv files already created.

All benchmarks can be ran at once using `./report/benchmarks-scripts/bm.sh gitlab-token first-csv sec-csv this-csv fourth-csv`. Only the memory benchmark on the original ALEX cannot be ran with it as it needs another repo and compilation using shell scripts and CMake can cause problems on some system.

N.B. All the scripts benchmarks were designed to be ran on a system that has two CPUs and two NUMA nodes.

# Report
The report can be obtain from the `./report/report.tex` file which uses the `refs.bib` file for references and the png images for plots. All the data used for the report is in `./report/graphs` which also contains the R markdown file `./report/graphs/graphs.Rmd` which was used to make the plots found in the report.