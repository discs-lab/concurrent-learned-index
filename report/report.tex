\documentclass{article}
\usepackage{graphicx} % Required for inserting images
\usepackage{listings}
\usepackage{biblatex}
\usepackage{geometry}
\geometry{a4paper, margin=1in}

\addbibresource{refs.bib}

\lstdefinestyle{mystyle}{
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=4
}

\lstset{style=mystyle}

\title{Concurrency In Learnned Indexes}
\author{Olivier Michaud 261052590}
\date{April 2023}

\begin{document}

\maketitle

\section{Introduction}\paragraph{}
Indexes are key structures of database systems to allow fast response time. Learned indexes structures are an emerging attempt at increasing performance of indexes. It was first introduced by Kraska et al \cite{KraskaEtAl}, who showed that it could perform better than traditional indexes using B+-trees. To develop on what was a read only data structure, Ding et al \cite{DingEtAl} came up with ALEX, an updatable adaptive learned index which would beat B+-tree by a great factor on performance and use orders of magnitude less memory. 
\paragraph{}
Then, among many points, Wongkham et al \cite{WongkhamEtAl} made it clear that learned indexes lack support for concurrency. Though they themselves implemented a concurrent version of ALEX, Christian Zhao found that it deadlocks in write workloads with over 1 billion keys \cite{ZhaoEmail} This report presents a new approach to concurrency in ALEX using finely grained locks and threads positional awareness. It was made possible using Christian Zhao report on ALEX \cite{ZhaoReport}.

\section{Background}
\paragraph{Structural overview}
ALEX uses a very similar structures to B+-trees, maintaining a very shallow tree with a large fanout. However, internal nodes do not keep the key ranges that would usually be used with comparison operations to determine which subtree to explore. Instead, each node keeps the two coefficients of a basic linear regression model to effectively compute the index of the children array in which to traverse. This series of linear models is called the recursive model index (RMI)
\paragraph{Nodes layout}
Both internal and leaf nodes keep children in a gapped array. It amortizes the costs of shifting when inserting new data and to maintain a low memory footprint, the array is dynamically expanded as needed, until reaching a set maximum size. Reaching the maximum size eventually triggers structural changes. Internal nodes fill all the positions of the array by duplicating pointers to children. Internal nodes fill all the gaps with children pointers based on the computed cumulative distribution function with aim of allowing leaf nodes to have a cumulative distribution function as linear as possible. Leaf nodes fill gaps with the value of the closest filled position with greater index to improve performance on exponential searches on the array.
\paragraph{Cost models}
Cost models rely on the average number of exponential search iterations needed to lookup a key and the average number of shifts needed for insert. They lay the basis to make decisions about structural changes in the tree to obtain more precise linear models. When those statistics are not known, they are estimated under the assumption that lookups follow a linear distribution and inserts respect the current key distribution in the tree.
\paragraph{Operations}
ALEX supports all expected standard operations. Read operations use the RMI to traverse to the lead node containing the key. They never perform any writes in the tree except updating the statistics used by the cost models. Insert operations are more complex and case specific. They first fetch the leaf proposed by the RMI. If inserting in the leaf is a simple operation, then it proceeds as such and returns. However, if the inserting cost is too expensive, then it will use the cost models to determine whether it should resize a node, split sideways or split downwards. Splitting sideways involves splitting the key domain based on the cost models and changing the duplicated pointers in the parent node to point to the two newly created nodes. Splitting downwards replace a leaf node with an internal node and creates two new leaf nodes. The parent will now pointer to the new internal node and the latter will have the new leaf nodes as children. 

\section{Description}
\subsection{Overview}
\paragraph{}
The approach to implementing concurrency in ALEX is as fine grained as possible. Read operations are more optimistic whereas write operations implement a pessimistic approach. It involves relaxations of the data structure where possible to maintain performance, but enforces strict barriers when needed to maintain data consistency. The main relaxation is on the number of lookups. The assumption is that even if some lookups are not counted due to two threads incrementing the counter at the same time, the model will provide an approximation that is good enough. On the other side, the number of inserts and the number of shifts are thread safe to maintain good performance on key placement when inserting.
\paragraph{}
Finely grained concurrency control is desired in our use case as it ensures that every operation that is blocking to other threads blocks the smallest amount of resources in the tree as possible. However, a good balance is necessary and hard to maintain as making the implementation more finely grained usually involves having more locks and other synchronization structures. It can greatly increase the memory footprint which is undesirable in the context where ALEX shines for its low memory footprint.
\paragraph{}
This implementation resembles the XIndex concurrency approach \cite{TangEtAl}. They also implemented per record locks. However, when a thread cannot acquire a record's lock, it is forwarded to another index, implemented using a B+-tree. We believe the retry based approach of the present implementation is preferable for lower memory footprint as it does not involve maintaining secondary data structures.
\paragraph{}
Contention has been a very present problem in the current implementation. We thought using higher level synchronization primitives like semaphores might perform better at handling contention on highly accessed counters. However, both sempahores and mutexes were respectively 5 and 10 times slower at handling concurrent accesses so they were discarded. As of now, we believe contention is at the core of what is preventing better scaling of the implementation.

\subsection{Node changes}
\paragraph{}
Both internal nodes and leaf nodes have the following two new fields in common. 
\begin{figure}
    \begin{lstlisting}[language=C++]
class Node {
    /* 
    other attributes 
    */
    
    atomic_int visitors = 0;
    atomic_int lock = 0;
}
    \end{lstlisting}
    \caption{Node class}
\end{figure}
Almost all traversal in the tree are done from top to bottom, except for traversal corrections that occur when the models deviate and a lookup ended up in the wrong leaf node, but even then, the correction is done by going back up and down again. Thus, the lock is used to safely lock the subtree where the locked node is the root since no sideways traversals can occur. The \verb|visitors| variable is a counter used to keep track of where threads are in the tree such that a thread locking a subtree knows when it can safely perform its tasks by waiting on the visitor count. In other words, the node lock is a one way filter that allows thread to leave the subtree but not enter it. Atomic variables are used at they leverage hardware instructions to allow greater performance.

\paragraph{}
Leaf nodes have the following added attributes as well:
\begin{figure}
    \begin{lstlisting}[language=C++]
class LeafNode {
    /*
    Node attributes
    other attributes
    */
    
    atomic_uint stats_lock = 0;
    atomic_uint bitmap_lock = 0;
    atomic_uint8 key_locks[data_capacity];
}
    \end{lstlisting}
    \caption{Leaf class}
\end{figure}
The \verb|stats_lock| variable is used to safely update the key domain of the node. No relaxation can be allowed on this since the key domain lays the basis to know if a lookup operation arrived in the right leaf node and a relaxation would mean lookups could be redirected to a neighbouring node even though the key was in this node. The \verb|bitmap_lock| variable is used to perform updates on the bitmap that keep tracks of which position in the arrays of keys and values are filled. The locking only blocks write operations on the map. Read operations are optimistic. They never block on the bitmap and will verify the consistency of their operations upon completion. The \verb|key_locks| variable is an array that keeps a lock for every position in the gapped array. It is used for finely grained concurrency control on insert operations. In this case as well read operations are optimistic and do not block on locked keys. Key locks are 8 bit unisgned integers as it smallest integer type such that there is a one to one mapping between the key locks array and the key array. This help reduce the memory footprint of the concurrency implementation to maintain the low memory cost for which ALEX was designed.

\subsection{Operations}
\subsubsection{Node visiting}
\begin{figure}
    \begin{lstlisting}[language=C++]
node.visit() {
    bool success <- false
    do {
        visitors++;
        success = true;
        if (lock) {
            visitors--
            success <- false
            while (lock) {}
        }
    } while (!success)
}
    \end{lstlisting}
    \caption{Node visit function}
\end{figure}
\paragraph{}
Figure 3 is a blocking function on a node's lock. Both internal and leaf nodes have it and it is used during tree traversals. It first increments the visitor counter and then checks if the node is actually locked. If it is, it decrements the visitor count and busy wait on the lock until it can try again. Operations are extremely fast in the ALEX so busy waits avoid the overhead of context switching that come with sleep waiting. During early attempts using standard non fair mutexes, on some operations a thread would complete its insert, unlock the mutex, come back for another operation and acquire the lock again before any other thread could get out of sleep and acquire it. 
\paragraph{}
This function should be seen as a locking function rather than an observer of the different threads position in the tree. That is, calling this function provides a certificate that as long a the thread is under that node on which it was called, no structural modifications can happen such that its path back up the tree would be broken or that a node being used by that thread would be deleted.
\newline

\subsubsection{Traversing the tree}
\begin{figure}
    \begin{lstlisting}[language=C++]
get_leaf(key) {
    cur <- root_node
    traversal <- {}
    while (!cur.is_leaf) {
        cur.visit()
        traversal.append(cur)
        cur <- cur.children[cur.predict(key)]
    }
    return cur, traversal
}
    \end{lstlisting}
    \caption{Traversal to leaf}
\end{figure}
\paragraph{}
Figure 4 is the function used to traverse the tree down to the leaf node containing it. Only the call to \verb|visit| has to be added to it. Sequentiality is, however, very important in this piece. Though this is a simplified version, the actual code is much more complex and the children is obtained at the first line of each iteration of the loop. As previously, \verb|visit| is a locking function so it had to be called before any other operation on the node can be performed such as obtaining the appropriate children from the model since calling visit after using the model allows a thread to lock the node and change the children layout making the obtain prediction useless. The \verb|traversal| variable returns the path that was used to traverse the tree. It is necessary as nodes do not keep a pointer to their parents so it is the only way to go back up and let decrement the visitor count such that structural modifications can be performed by other threads. The leaf is returned separately from the traversal. When inserting, it can happen that the leaf is dropped so it avoids having to fix the traversal path. However, this leaves the responsibility of visiting the leaf to the client calling the function.
\newline

\subsubsection{Read operation}
\begin{figure}
    \begin{lstlisting}[language=C++]
get(key) {
    leaf, traversal <- get_leaf(key)
    leaf.visit()
    idx <- leaf.find_key(key)
    payload <- null
    if (idx >= 0) {
        payload <- leaf.get_payload(idx)
    }
    unvisit_tree(traversal, leaf)
    return payload
}
    \end{lstlisting}
    \caption{Function to get a key's payload}
\end{figure}
\paragraph{}
In the get function in figure 5, the \verb|get| function does exactly as expected: it finds the leaf that would contain the key, visits it to respect sequentiality and then obtains the payload from the leaf. Once all operations on the leaf are completed, it unvisits the tree from the bottom up to the root by simply decrementing the visitor count on the path. Then it returns the value associated with the key or null if there was no matching key.
\newline

\subsubsection{Write operation}
\begin{figure}
    \begin{lstlisting}[language=C++]
leaf_insert(key, payload) {
	
	if (need_resize()) {
		success <- resize()
		if (success == -1) {
			return 4, -1
		}
	}
	
	insert_pos <- find_insert_position(key)
	low_pos <- find_left_neighbor(insert_pos)
	
	// locks range (low_pos, insert_pos] 
	success <- lock_key_region(low_pos, insert_pos)
	if (success == -1) {
		return 5, -1
	}
	
	/* standard insert procedure */
	
	unlock_key_region(low_pos, insert_pos)
	return 0, insert_pos
}
    \end{lstlisting}
    \caption{Function to insert in a leaf}
\end{figure}
\paragraph{}
The \verb|leaf_insert| function in figure 6 is used when the appropriate leaf has been found. It first checks if resizing is necessary using a set expansion threshold. If it is, the calling thread will attempt to lock the leaf. If locking fails, it means another thread acquired it first. Then, the calling thread will return with an error flag. 
\paragraph{}
Once the resizing is complete, it finds the inserting position according to the model, adjusts it to the current context in the array. Since the gapped position of the key arrays are filled with the value of the closest filled position with greater index, inserting in the array means we may have to update certain positions to the left of the insertion position. Thus, a region of keys must be locked in order to safely perform the update. The policy used is to always lock from the largest index going down and cancelling the whole operation if an already locked key is encountered. Doing so avoids deadlocks and starvation since at any point, the array is bounded such that at least one thread is going to it obtain all the locks down to index zero and succeed with its operation. Once insertion is done, to be consistent with the locking step, it unlocks the key region from the left to the right.
\newline

\begin{figure}
    \begin{lstlisting}[language=C++]
insert(key, payload) {
    
    RETRY
    
    leaf, traversal <- get_leaf(key)
    leaf.visit()
    fail, insert_pos <- leaf.insert(key, payload)
    // locked node or locked key region
    if (fail == 4 || fail == 5) {
        unvisit_tree(traversal, leaf)
        goto RETRY
    }
    
    while (fail) {
        parent <- traversal.back()
        choice <- choose_structural_choice()
        success <- 0
        switch choice {
            case 1: success <- leaf.resize()
            case 2: success <- split_sideways()
            case 3: success <- split_downwards()
        }
        if (success == -1) {
            unvisit_tree(traversal, leaf)
            goto RETRY
        }
        fail, insert_pos <- leaf.insert(key, payload)
        if (fail == 4 || fail == 5) {
            unvisit_tree(traversal, leaf)
            goto RETRY
        }
    }
    
    unvisit_tree(traversal, leaf)
    return insert_pos
}
    \end{lstlisting}
    \caption{Function to insert in the tree}
\end{figure}
\paragraph{}
The inserting function that is in figure 7 is a simplified version that outlines the key concepts of the concurrency implementation. It first traverses the tree using the mechanisms described earlier and directly attempts an insert in the obtained leaf node. If it succeeds, it unvisits the tree and returns the position at which the key was inserted.
\paragraph{}
If leaf inserting fails, then it falls back to using the cost models to know what to do. This leads to the three possible actions on lines 19-21. The \verb|leaf.resize| function is the same as used in figure 6. If a split is required, the thread will attempt to lock the leaf's parent node. If it succeeds in acquiring the lock, it will wait until all visiting threads have left by busy waiting on the \verb|visitors| counter introduced in figure 1. Once the subtree is empty from other threads it will perform its operations and free the parent node once it is done. If any of those operations fail, it is due to another thread acquiring the lock first. Thus, to let the winning thread perform its operation, the thread that failed will receive a \verb|-1| flag, unvisit the tree and return to the retry point to start over.

\section{Experiments}
\subsection{Set up}
\paragraph{}
The following experiments were all performed using the following set up:
\begin{itemize}
    \item Hardware:
    \begin{itemize}
        \item Dual Intel Xeon Gold 6354 CPU (36 cores in total)
        \item One 500GB NUMA node per CPU.
    \end{itemize}
    \item Software:
    \begin{itemize}
        \item Ubuntu 20.04.6 LTS
        \item gcc version 9.4.0
    \end{itemize}
\end{itemize}
Unless otherwise stated, the command \verb|numactl| was used to bind the processes on a specific CPU and its corresponding NUMA node to avoid the large overhead of inter socket memory synchronization.

\subsection{Data}
\paragraph{}
The following four datasets of 8 bytes keys were used for experiments: \verb|unif-data-500M|, \verb|ycsb|, \verb|fb| and \verb|unif-data-10B|. The first is a simple shuffled list of the first 500 million integers used a uniform workload by sampling a part of it. \verb|YCSB| is the workload that was used by Ding et al. \cite{DingEtAl} to test the original version of ALEX. It was generated according to the YCSB benchmark \cite{CooperEtAl} and is rather uniform as well. \verb|facebook| is a real dataset of upsampled Facebook user id \cite{WongkhamEtAl}, which is known to be usually challenging on learned index due to a small number of extreme outliers that affect the cost models of the index \cite{MaltryAndDittrich}. The last one, \verb|unif-data-10B| is also a shuffled list, but on the first 10 billion integers. It was used to observe memory footprint and stress test the index on very large amounts of data as this was the problem with Wongkham et al's concurrent implementation of ALEX \cite{ZhaoEmail}. It was not used for other benchmarks on uniform workloads due to the very large size of the file that made benchmarks long to initialize. 

\subsection{Throughput}
\paragraph{}
The following three plots show the throughput of the learned index on an increasing number of threads for \verb|unif-data-500M|, \verb|ycsb| and \verb|fb|. They are read-write ratio where the write ratio can be obtain from $1 - read ratio$. All keys were matched with an 8 bytes payload. The tree was bulk loaded with $(100,000,000 - write\_ratio \cdot 100,000,000)$ keys except for write only workloads where one thousand keys were bulk loaded. All benchmarks were performed on both a uniform and Zipfian distribution on the read operations (respectively contracted to unif and zipf for readability on the plots).

\begin{figure}
    \centering
    \includegraphics[scale=0.60]{unif_throughput.png}
    \caption{Benchmark on unif-data-500M}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.60]{ycsb_throughput.png}
    \caption{Benchmark on ycsb}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.60]{fb_throughput.png}
    \caption{Benchmark on fb}
\end{figure}
\paragraph{}
Figure 8 and 9 show how low number of threads can leverage cache locality when the data is relatively uniform and the read distribution is Zipfian. However, the drop in throughput between using one and two threads shows how contention on the atomic variables impacts performance. 
\paragraph{}
On insert only workloads where the access distribution is irrelevant, we can observe how high number of threads yield very low increase in performance. It suggests that locking subtrees combined with contention on atomic variable is a high bottleneck. However, read heavy workloads scale interestingly well even on harder datasets such as \verb|fb|.
\paragraph{}
Moreover, figure 8 and 9 show how on uniform and close to uniform workload performance do not scale as fast when increasing the number of threads. This is most likely due to leaves being almost all at the same depth in such workloads, leading to all operations roughly taking the same amount of time to complete. Thus, all the threads always come back at the same time on the high contention zones of the tree which are the root and super root nodes, the single entry point that all threads must use for traversal. Workloads on \verb|fb| seem to much less affected, which would make sense since the higher disparity in the dataset create operations that take various amounts of time and offset arrivals on high contention zones.

\subsection{Memory}
Memory benchmarks were done on insert only workloads. One thousand keys were bulk loaded in the tree before starting the benchmark, and then it would insert until reaching the defined maximum number of keys. The values obtained for ALEX are single threaded whereas the concurrent implementation used 36 threads. The number of bytes per key is computed as $(sizeof(tree) - sizeof(payload) \cdot number\_keys) / number\_keys$.
\begin{figure}
    \centering
    \includegraphics[scale=0.60]{mem_footprint.png}
    \caption{Memory footprint of ALEX and the concurrent implementation}
\end{figure}

Interestingly, figure 11 shows that ALEX maintains a very constant memory consumption on the different number of keys and datasets compared to the concurrent implementation. This proposes that as the tree grows, less conflicts occur on the relaxed statistics used for the cost models, allowing more accurate models that get closer to what ALEX does. For smaller number of keys, we observe up to 4 more bytes per key in the concurrent version, but it goes down to about 1.5 on larger workloads, probably due to better optimized insertions allowed by more accurate models leading to a better use of the gapped array. Overall, the concurrent maintains ALEX's property of being an index that uses a small amount of memory.

\begin{figure}
    \centering
    \includegraphics[scale=0.60]{stress_mem_footprint.png}
    \caption{Memory footprint on the concurrent implementation}
\end{figure}

Figure 12 serves two purposes. It shows how the memory consumption of the concurrent implementation manages to remain relatively constant on uniform workloads even with the relaxed statistics used by the cost models. Also, it demonstrates that this implementation remains functional on large datasets compared to Wongkham et al's concurrent implementation \cite{ZhaoEmail}.  

\section{Issues}
\subsection{Contention}
As previously mentioned, there is very high contention on the root and the super root nodes which serve as the sole entry point to the tree. This bottlenecks scaling on high number of threads.

\subsection{Memory allocation}
The current implementation suffers from two problems on memory operations. First, allocations of atomic variables in C++17 did not allow automatic zero-initialization. Thus, in the rare event where the allocator reuses previously freed memory, key locks are not initialized to 0 as they should but rather take the values left in memory. This is a rare event as usually the memory consumption only increased in the benchmarks that were performed. This seems to have been fixed in C++20 so upgrading the project should be possible.
\paragraph{}
There is also an allocation problem that occurs in less than 1\% of benchmarks where the allocator does not properly instantiate objects or will return \verb|std::bad_alloc| when using the deconstructor of an object. It is currently unknown what is causing this, but early analysis shows that no other threads could access the object being freed to it likely that it is not caused by the concurrent implementation of the index.

\subsection{Multi-CPU systems}
We observed that the memory synchronization required by atomic variables has a very high overhead on systems that use more than one CPU. It is so expensive that running the benchmark with 64 threads spread across two CPUs yields a lower throughput than running the same workload on 32threads or even sometimes 16 threads limited to a single CPU.

\section{Future work}
ALEX has previously been optimized for lower tail latency in a work called TONE \cite{ZhangEtAl}. TONE uses a two-level gapped array in leaf nodes. This design effectively eliminates the expansion operation on leaf nodes. Implementing the current approach to concurrency in TONE should simply require to change the array of key locks to a two-level array of key locks to follow TONE's structure. The rest of the implementation should be exactly the same.
\paragraph{}
We are hoping this concurrent design will allow future work to implement ALEX as a learned index in system used in the industry such as RocksDB. As Zhang et al shown, RocksDB skiplist has a much larger memory footprint than ALEX\cite{ZhangEtAl} so replacing it with a concurrent version of ALEX would greatly improve memory usage.

\section{Conclusion}
In conclusion, we succeeded at developing a concurrent implementation of ALEX that allows scaling whilst maintaining its characteristic low memory footprint and allowing a certain amount of scaling if we stick to single CPU machines and we believe it is a step in the right direction to solve the point about concurrency presented by Wongkham et al \cite{WongkhamEtAl}. Observations on benchmarks propose high contention on the super root and root node, which we hypothesize could be improved by replicating the super root and root nodes, thus spreading contention over multiple variables. Delete operations remain to be implemented to make this concurrent index fully functional, but we believe the current approach to concurrency can be extended to such operations as well. Last, as presented in the Issues section, this concurrent learned index does not scale when using multiple CPUs and it is unclear if the current implementation could be extended to such systems.

\newpage

\printbibliography


\end{document}
