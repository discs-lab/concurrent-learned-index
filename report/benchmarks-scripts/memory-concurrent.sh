#!/bin/bash

gitlabToken=$1
outputFile=$2

data_files=(
      "/tmp/om-learned-index/alex-concurrent/datasets/fb"
      "/tmp/om-learned-index/alex-concurrent/datasets/unif-data-500M.bin.data"
      "/tmp/om-learned-index/alex-concurrent/datasets/ycsb-200M.bin.data"
)

allThreads=(36)

nbBenchmarks=4
benchmarks=(
      0 1 9999000 10000000 0.0001
      0 1 99999000 100000000 0.00001
      0 1 199999000 200000000 0.000005
)

nbReplicas=5
dist="uniform"

for dataset in ${data_files[@]}; do
        for ((i=0;i<$nbBenchmarks;i++)) do
                readRatio=${benchmarks[i * 5]}
                insertRatio=${benchmarks[i * 5 + 1]}
                opNumber=${benchmarks[i * 5 + 2]}
                table_size=${benchmarks[i * 5 + 3]}
                initTableRatio=${benchmarks[i * 5 + 4]}
                for t in ${allThreads[@]}; do
                        for ((j=0;j<$nbReplicas;j++)) do
                                numactl --membind=0 --cpunodebind=0 ./build/microbench --keys_file=$dataset --keys_file_type=binary --read=$readRatio --insert=$insertRatio --operations_num=$opNumber --table_size=$table_size --init_table_ratio=$initTableRatio --thread_num=$t --index=alex --sample_distribution=$dist --output_path=$outputFile --memory=true
                        done
                done
        done
        bash ./report/benchmarks-scripts/gitlab-autosave.sh $gitlabToken $outputFile
done