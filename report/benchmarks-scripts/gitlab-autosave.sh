#!/bin/bash

gitlabToken=$1
csvFile=$2
timestamp=$(date +%s)
tmpFile=tmp_"$timestamp"_alex_c_benchmark.json
dateUtc=$(date -u)
csvFileContent=$(cat $csvFile | awk '{printf "%s\\n", $0}')
touch $tmpFile

echo "{
        \"branch\":\"main\",
        \"commit_message\":\"$dateUtc\",
        \"actions\": [
                {
                        \"action\":\"update\",
                        \"file_path\":\"$csvFile\",
                        \"content\":\"$csvFileContent\"
                }
        ]
}" >> $tmpFile

repoId=0
gitlabUrl="https://gitlab.cs.mcgill.ca/api/v4/projects/$repoId/repository/commits"

curl -X POST --header "PRIVATE-TOKEN: $gitlabToken" --header "Content-Type: application/json" --data @$tmpFile "$gitlabUrl"

rm $tmpFile