#!/bin/bash

./report/benchmarks-scripts/workloads-unif.sh $1 $2

./report/benchmarks-scripts/workloads-zipf.sh $1 $3

./report/benchmarks-scripts/memory-concurrent.sh $1 $4

./report/benchmarks-scripts/stress-test.sh $1 $5