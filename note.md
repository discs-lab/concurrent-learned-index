
# CMake dependencies

sudo apt-get install intel-mkl-full
sudo apt-get install libjemalloc-dev
sudo apt install libtbb-dev

# Performance survey tools

sudo apt install linux-tools-common
sudo apt install linux-intel-iotg-5.15-tools-common
sudo apt install linux-tools-5.4.0-139-generic
