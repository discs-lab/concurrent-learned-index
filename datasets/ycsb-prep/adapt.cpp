#include<cstdint>
#include <iostream>
#include<fstream>

int main() {
    std::ifstream ycsbFile("ycsb-200M.bin");
    char *keys = new char[1600000000];
    ycsbFile.read(keys, 1600000000);
    uint64_t *size = new uint64_t;
    *size = 200000000;
    std::ofstream adaptedYcsb("ycsb-200M.bin.data");
    adaptedYcsb.write((const char*)(size), 8);
    adaptedYcsb.write(keys, 1600000000);
    ycsbFile.close();
    adaptedYcsb.close();
}