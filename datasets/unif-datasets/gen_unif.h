#pragma once

#include<algorithm>
#include<cstdint>
#include <iostream>
#include<fstream>
#include<random>

void gen_unif_500M() {
    uint64_t nb_keys = 500000000;
    uint64_t *keys = new uint64_t[nb_keys + 1];
    keys[0] = nb_keys;
    for (uint64_t i = 1; i <= nb_keys; i++) {
        keys[i] = i;
    }
    std::shuffle((keys + 1), (keys + nb_keys + 1), std::default_random_engine(1234));
    std::shuffle((keys + 1), (keys + nb_keys + 1), std::default_random_engine(5678));
    std::ofstream data_file = std::ofstream("unif-data-500M.bin.data");
    data_file.write((const char*)(keys), (nb_keys + 1) * sizeof(uint64_t));
}

void gen_unif_10B() {
    uint64_t nb_keys = 10000000000;
    uint64_t *keys = new uint64_t[nb_keys + 1];
    keys[0] = nb_keys;
    for (uint64_t i = 1; i <= nb_keys; i++) {
        keys[i] = i;
    }
    std::shuffle((keys + 1), (keys + nb_keys + 1), std::default_random_engine(1234));
    std::shuffle((keys + 1), (keys + nb_keys + 1), std::default_random_engine(5678));
    std::ofstream data_file = std::ofstream("unif-data-10B.bin.data");
    data_file.write((const char*)(keys), (nb_keys + 1) * sizeof(uint64_t));
}