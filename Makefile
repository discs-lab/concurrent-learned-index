comp:
	cd build
	cmake -DCMAKE_BUILD_TYPE=Release .. && make
	cd ..

run:
	./build/microbench --keys_file=/home/om/COMP400/ALEX-concurrent/datasets/fb --keys_file_type=binary --read=0 --insert=1 --operations_num=80000 --table_size=100000 --init_table_ratio=0.2 --thread_num=1 --index=alex